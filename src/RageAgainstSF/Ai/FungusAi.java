package RageAgainstSF.Ai;

import RageAgainstSF.Creature;
import RageAgainstSF.EnvironmentFactory;

public class FungusAi extends CreatureAi {

	private EnvironmentFactory factory;
	private int spreadCount;
	
	public FungusAi(Creature creature, EnvironmentFactory factory) {
		super(creature);
		this.factory = factory;
	}

	@Override
	public void onUpdate() {
		if(spreadCount < 10 && Math.random() < 0.01)
			spread();
		if(Math.random() > 0.95)
			makeSounds();
	}
	
	private void spread() {
		int x = creature.x + (int)(Math.random() *11) - 5;
		int y = creature.y + (int)(Math.random() *11) - 5;
		
		if(!creature.canEnter(x, y, creature.z))
			return;
		
		creature.doAction("spawn a child");
		
		Creature child = factory.newFungusChild(creature.z);
		
		child.x = x;
		child.y = y;
		child.z = creature.z;
		
		spreadCount++;
	}
	
	@Override
	public void makeSounds() {
		creature.doAction("make some crunching sound");
	}
	

}
