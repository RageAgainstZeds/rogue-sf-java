package RageAgainstSF.Ai;

import RageAgainstSF.Creature;

public class FungusChildAi extends CreatureAi {
	
	public FungusChildAi(Creature creature) {
		super(creature);
	}

	@Override
	public void onUpdate() {
		if(Math.random() > 0.95)
			makeSounds();
	}
	
	@Override
	public void makeSounds() {
		creature.doAction("make some grumble sound");
	}
	

}
