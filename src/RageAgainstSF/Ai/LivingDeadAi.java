package RageAgainstSF.Ai;

import java.util.List;

import RageAgainstSF.Creature;
import RageAgainstSF.CreatureSound;
import RageAgainstSF.Path;
import RageAgainstSF.Point;

public class LivingDeadAi extends CreatureAi {

	private Creature player;
	
	
	
	public LivingDeadAi(Creature creature, Creature player) {
		super(creature);
		this.player = player;
	}

	@Override
	public void onUpdate() {
		if(Math.random() < 0.2)
			return;
		
		if(Math.random() < 0.1)
			makeSounds();
		
		if(creature.canSee(player.x, player.y, player.z))
			hunt(player);
		else
			wander();
	}

	private void hunt(Creature target) {
		List<Point> points = new Path(creature, target.x, target.y).points();
		
		int mx = points.get(0).x - creature.x;
		int my = points.get(0).y - creature.y;
	  
	    creature.moveBy(mx, my, 0);
	}

	@Override
	public void makeSounds() {
		creature.doAction(" whisper " + CreatureSound.getLivingDeadSound());
	}
	
}
