package RageAgainstSF.Ai;

import RageAgainstSF.Creature;
import RageAgainstSF.CreatureSound;

public class RockerOnDrugsAi  extends CreatureAi {
	public RockerOnDrugsAi(Creature creature){
		super(creature);
	}
	
	public void onUpdate() {
		wander();
		wander();
		if(Math.random() < 0.15)
			makeSounds();
	}
	
	@Override
	public void makeSounds() {
		creature.doAction("shout " + CreatureSound.getRockerSound());
	}
}
