package RageAgainstSF.Ai;

import RageAgainstSF.Creature;
import RageAgainstSF.EnvironmentFactory;

public class StinkingHoleAi extends CreatureAi {
	private EnvironmentFactory factory;
	private Creature player;
	
	public StinkingHoleAi(Creature creature, EnvironmentFactory factory, Creature player) {
		super(creature);
		this.factory = factory;
		this.player = player;
	}

	@Override
	public void onUpdate() {
		if(creature.canSee(player.x, player.y, player.z)) {
			if(Math.random() < 0.05)
				breed();
		}
		
	}
	
	private void breed() {
		int x = creature.x + (int)(Math.random() * 2) - 1;
		int y = creature.y + (int)(Math.random() * 2) - 1;
		
		if(!creature.canEnter(x, y, creature.z))
			return;
		
		creature.doAction("breed a bloody bastard");
		
		Creature child = factory.newRockerOnDrugs(creature.z);
		
		child.x = x;
		child.y = y;
		child.z = creature.z;
	}
}
