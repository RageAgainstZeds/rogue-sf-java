package RageAgainstSF;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import RageAgainstSF.Ai.CreatureAi;

public class Creature {
private World world;
	
	public int x;
	public int y;
	public int z;
	
	private char glyph;
	public char glyph() { return glyph; }
	
	private Color color;
	public Color color() { return color; }

	private CreatureAi ai;
	public void setCreatureAi(CreatureAi ai) { this.ai = ai; }
	
	//hit points
	private int maxHp;
	public int maxHp() { return maxHp; }
	private int hp;
	public int hp() { return hp; }
	
	//attack/defense
	private int attackValue;
	public int attackValue() { 
		return attackValue
				+ (weapon == null ? 0 : weapon.attackValue())
				+ (armor == null ? 0 : armor.attackValue())
				+ (boots == null ? 0 : boots.attackValue())
				+ (helmet == null ? 0 : helmet.attackValue())
				+ (gloves == null ? 0 : gloves.attackValue())
				+ getRingsAttackSum();
	}
	
	private int defenseValue;
	public int defenseValue() { 
		return defenseValue
				+ (weapon == null ? 0 : weapon.defenseValue())
				+ (armor == null ? 0 : armor.defenseValue())
				+ (boots == null ? 0 : boots.defenseValue())
				+ (helmet == null ? 0 : helmet.defenseValue())
				+ (gloves == null ? 0 : gloves.defenseValue())
				+ getRingsDefenseSum();
	}
	
	//vision
	private int visionRadius;
	public int visionRadius() { 
		return visionRadius
				+ (helmet == null ? 0 : helmet.visionValue())
				+ getRingsVisionSum();
	}
	
	//creature name
	private String name;
    public String name() { return name; }
    
    //creature's Inventory
    private Inventory inventory;
    public Inventory inventory() { return inventory; }
    
    //food stuff
    private double maxFood;
	public double maxFood() { return maxFood; }

	private double food;
	public double food() { return food; }
	
	//weapon and armor
	private Item weapon;
	public Item weapon() {return weapon;}
	
	private Item armor;
	public Item armor() {return armor;}
	
	private Item boots;
	public Item boots() {return boots;}
	
	private Item helmet;
	public Item helmet() {return helmet;}

	private List<Item> rings;
	public List<Item> rings() {return rings;}
	
	private Item gloves;
	public Item gloves() {return gloves;}
	
	//creature XP
	private int xp;
	public int xp() { return xp; }
	
	private int level;
	public int level() {return level;}
	
	public void modifyXp(int amount) {
		xp += amount;
		notify("You %s %d xp.", amount < 0 ? "lose" : "gain", amount);
		while(xp > (int)(Math.pow(level, 1.5) * 20)) {
			level++;
			doAction("advance level to %d",  level);
			ai.onGainLevel();
			modifyHp((int)((maxHp() - hp()) * 0.5));
		}
	}
	
	
	
	
	
	public Creature(World world, char glyph, Color color, int maxHp, int attack, int defense, int visionRadius, String name){
		this.world = world;
		this.glyph = glyph;
		this.color = color;
		this.maxHp = maxHp;
		this.hp = maxHp;
		this.attackValue = attack;
		this.defenseValue = defense;
		this.visionRadius = visionRadius;
		this.name = name;
		this.inventory = new Inventory(20);
		this.maxFood = 1000;
		this.food = maxFood / 3 * 2;
		this.rings = new ArrayList<Item>();
		this.level = 1;
	}
	
	public void moveBy(int mx, int my, int mz){
        if(mx == 0 && my == 0 && mz == 0)
        	return;
		
		Tile tile = world.tile(x+mx, y+my, z+mz);
    
        if (mz == -1){
            if (tile == Tile.STAIRS_DOWN) {
            	modifyFood(-1);
                doAction("walk up the stairs to level %d", z+mz+1);
            } else {
                doAction("try to climb up but are stopped by the cave ceiling");
                return;
            }
        } else if (mz == 1){
            if (tile == Tile.STAIRS_UP) {
            	modifyFood(-1);
                doAction("walk down the stairs to level %d", z+mz+1);
            } else {
                doAction("try to go down there is no hole in the floor");
                return;
            }
        }
    
        Creature other = world.creature(x+mx, y+my, z+mz);
    
        if (other == null)
            ai.onEnter(x+mx, y+my, z+mz, tile);
        else
            attack(other);
    }

	public void attack(Creature other){
		int amount = Math.max(0, attackValue() - other.defenseValue());
		
		amount = (int)(Math.random() * amount) + 1;
		
		doAction("attack the %s for %d damage", other.name, amount);
		other.modifyHp(-amount);
		
		if (other.hp < 1)
			gainXp(other);
		
		modifyFood(-0.5);
	}
	
	public void gainXp(Creature other){
	    int amount = other.maxHp
	      + other.attackValue()
	      + other.defenseValue()
	      - level * 2;

	    if (amount > 0)
	      modifyXp(amount);
	 }

	public void modifyHp(int amount) { 
		hp += amount;
		
		if (hp < 1) {
			doAction("die");
			leaveCorpse();
			world.remove(this);
		}
	}
	
	private void leaveCorpse() {
		Item corpse = new Item('%', color, name + " corpse");
	    corpse.setFoodValue(maxHp * 2);
	    world.addAtEmptySpace(corpse, x, y, z);
	}

	public void dig(int wx, int wy, int wz) {
		world.dig(wx, wy, wz);
		modifyFood(-20);
		doAction("dig through rock");
	}
	
	public void update(){
		modifyFood(-0.5);
		ai.onUpdate();
	}

	public boolean canEnter(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz).isGround() && world.creature(wx, wy, wz) == null;
	}

	public void notify(String message, Object ... params){
		ai.onNotify(String.format(message, params));
	}
	
	public void doAction(String message, Object ... params){
		int r = 9;
		for (int ox = -r; ox < r+1; ox++){
			for (int oy = -r; oy < r+1; oy++){
				if (ox*ox + oy*oy > r*r)
					continue;
				
				Creature other = world.creature(x+ox, y+oy, z);
				
				if (other == null)
					continue;
				
				if (other == this)
					other.notify("You " + message + ".", params);
				else
					other.notify(String.format("The %s %s.", name, makeSecondPerson(message)), params);
			}
		}
	}
	
	private String makeSecondPerson(String text){
		String[] words = text.split(" ");
		words[0] = words[0] + "s";
		
		StringBuilder builder = new StringBuilder();
		for (String word : words){
			builder.append(" ");
			builder.append(word);
		}
		
		return builder.toString().trim();
	}
	
	public boolean canSee(int wx, int wy, int wz) {
		return ai.canSee(wx, wy, wz);
	}
	
	public Tile tile(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
            return world.tile(wx, wy, wz);
        else
            return ai.rememberedTile(wx, wy, wz);
	}
	
	public Tile realTile(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz);
	}

	public void pickup(){
        Item item = world.item(x, y, z);
    
        if (inventory.isFull() || item == null){
            doAction("grab at the ground");
        } else {
            doAction("pickup a %s", item.name());
            inventory.add(item);
            world.remove(x, y, z);
            
            modifyFood(-0.05);
        }
    }
	
	public void drop(Item item){
        if(world.addAtEmptySpace(item, x, y, z)) {
        	doAction("drop a " + item.name());
            inventory.remove(item);
            unequip(item);
        } else {
        	notify("There's nowhere to drop the %s.", item.name());
        }
    }

	public void modifyFood(double amount) {
	    food += amount;
	    if (food > maxFood) {
	        food = maxFood;
	        notify("Your belly can't hold that much!");
	        notify("You are going to puke. Your stomach is hurt!");
	        modifyHp(-2);
	        
	    } else if (food <= 0 && isPlayer()) {
	        notify("You are dying from starving.");
	    	modifyHp(-1000);
	    }
	}

	public boolean isPlayer(){
	    return glyph == '@';
	}
	
	public void eat(Item item){
		if (item.foodValue() < 0)
			notify("Gross!");
		
	    modifyFood(item.foodValue());
	    unequip(item);
	    inventory.remove(item);
	}
	
	public void equip(Item item) {
		if (item.attackValue() == 0 && item.defenseValue() == 0)
			return;
		
		switch(item.type()) {
			case "armor":
				unequip(armor);
				doAction("wield a " + item.name());
				armor = item;
				break;
			case "weapon":
				unequip(weapon);
				doAction("wield a " + item.name());
				weapon = item;
				break;
			case "boots":
				unequip(boots);
				doAction("put on pair of a " + item.name() + " on your feet");
				boots = item;
				break;
			case "gloves":
				unequip(gloves);
				doAction("attire pair of a " + item.name() + " on your hands");
				gloves = item;
				break;
			case "helmet":
				unequip(helmet);
				doAction("clothe a " + item.name() + " on your head");
				helmet = item;
				break;
			case "ring":
				if(rings.size() < 8) {
					rings.add(item);
					doAction("put up a " + item.name() + " on your finger");
				} else {
					unequip(rings);
					rings.add(item);
					doAction("put up a " + item.name() + " on your finger");
				}
				break;
		}
		
	}

	private void unequip(Item item) {
		if (item == null)
	         return;
	  
	      if (item == armor){
	          doAction("remove a " + item.name());
	          armor = null;
	      } else if (item == weapon) {
	          doAction("put away a " + item.name());
	          weapon = null;
	      } else if (item == boots) {
	          doAction("put away a " + item.name());
	          boots = null;
	      } else if (item == helmet) {
	          doAction("put away a " + item.name());
	          helmet = null;
	      } else if (item == gloves) {
	          doAction("put away a " + item.name());
	          gloves = null;
	      }
	}
	
	//for unequipping last equipped ring
	private void unequip(List<Item> item) {
		if (item == null)
	         return;
		rings.remove(rings.size()-1);
	}

	public void unequipRing(Item item) {
		rings().remove(item);
	}
	
	//calculate ring stats
	private int getRingsAttackSum() {
		int sum = 0;
		for (Item item : rings) {
			sum += item.attackValue();
		}
		return sum;
	}
	private int getRingsDefenseSum() {
		int sum = 0;
		for (Item item : rings) {
			sum += item.defenseValue();
		}
		return sum;
	}
	private int getRingsVisionSum() {
		int sum = 0;
		for (Item item : rings) {
			sum += item.visionValue();
		}
		return sum;
	}
	
	
	
	//levelUp upgrades
	public void gainMaxHp() {
	    maxHp += 10;
	    hp += 10;
	    doAction("look healthier");
	  }

	  public void gainAttackValue() {
	    attackValue += 2;
	    doAction("look stronger");
	  }

	  public void gainDefenseValue() {
	    defenseValue += 2;
	    doAction("look tougher");
	  }

	  public void gainVision() {
	    visionRadius += 1;
	    doAction("look more aware");
	  }

	  
	  //hero stats info on screen
	public String details() {
		return String.format("     level:%d     attack:%d     defense:%d     hp:%d", level, attackValue(), defenseValue(), hp);
	}
	
	public Creature creature(int wx, int wy, int wz) {
        if (canSee(wx, wy, wz))
            return world.creature(wx, wy, wz);
        else
            return null;
    }

	public Item item(int wx, int wy, int wz) {
        if (canSee(wx, wy, wz))
            return world.item(wx, wy, wz);
        else
            return null;
    }
	
	public void throwItem(Item item, int wx, int wy, int wz) {
        Point end = new Point(x, y, 0);
    
        for (Point p : new Line(x, y, wx, wy)){
            if (!realTile(p.x, p.y, z).isGround())
                break;
            end = p;
        }
    
        wx = end.x;
        wy = end.y;
    
        Creature c = creature(wx, wy, wz);
    
        if (c != null)
            throwAttack(item, c);
        else
            doAction("throw a %s", item.name());
    
        unequip(item);
        inventory.remove(item);
        world.addAtEmptySpace(item, wx, wy, wz);
    }

	private void throwAttack(Item item, Creature other) {
		modifyFood(-1);
	    
        int amount = Math.max(0, attackValue / 2 + item.thrownAttackValue() - other.defenseValue());
    
        amount = (int)(Math.random() * amount) + 1;
    
        doAction("throw a %s at the %s for %d damage", item.name(), other.name, amount);
    
        other.modifyHp(-amount);
    
        if (other.hp < 1)
            gainXp(other);
	}
	
}
