package RageAgainstSF;

import java.util.Arrays;
import java.util.List;

public class CreatureSound {
	
	private static List<String> rockerSounds = Arrays.asList("I'm on the highway to HELL!", "WE WILL ROCK YOU!", "MONSTERS ROCKING IN A FREE WORLD!"
			, "WE WANT A ROCK!", "I need some rock-stuff. GIMME!", "I WANNA A ROCK!", "I do cocaine!", "Prepare your buns!", "I'll kick You in the guts! WAAAGGHH!"
			, "Evrywun knows ROCK is best!", "I hear the sound of ROCK", "ROCK IS DA BEST!! WAAAAAGGGHH!!", "I need drugs! NOW!!"
			, "Check out my new T-shirt with band Mirvanna!");

	private static List<String> livingDeadSounds = Arrays.asList("Aaargh...", "Moost pheed mu tummy...", "Mummy... your lil boy s hungry...", "Fleshhh..."
			, "Pheeel bugs inside of me...mmm make it stop...", "Agonieee...", "Hhhrump...", "I c-cun't ppheel m-myyy... stomatchh...", "Kill me... hurry..."
			, "My arm... I ate my pricious arm...", "Grrrroooovvvy...", "Paaaaaain...", "My l-life... is agony...", "Make me stop... existing...", "Ohhh, found a coin!"
			, "Wanna bite you... a l-little...");
	
	
	public static String getRockerSound() {
		return '\'' + rockerSounds.get((int) (Math.random() * rockerSounds.size())) + '\'';
	}
	
	public static String getLivingDeadSound() {
		return '\'' + livingDeadSounds.get((int) (Math.random() * livingDeadSounds.size())) + '\'';
	}
}