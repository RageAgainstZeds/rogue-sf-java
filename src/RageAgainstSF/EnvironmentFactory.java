package RageAgainstSF;

import java.awt.Color;
import java.util.List;

import RageAgainstSF.Ai.RockerOnDrugsAi;
import RageAgainstSF.Ai.StinkingHoleAi;
import RageAgainstSF.Ai.AssasinAi;
import RageAgainstSF.Ai.FungusAi;
import RageAgainstSF.Ai.FungusChildAi;
import RageAgainstSF.Ai.LivingDeadAi;
import RageAgainstSF.Ai.PlayerAi;
import asciiPanel.AsciiPanel;

public class EnvironmentFactory {
	private World world;

	public EnvironmentFactory(World world) {
		this.world = world;
	}
	
	//CREATING PLAYER
	public Creature newPlayer(List<String> messages, FieldOfView fov) {
		Creature player = new Creature(world, '@', AsciiPanel.brightWhite, 100, 20, 5, 9, "Mr. Torgue");
		world.addAtEmptyLocation(player, 0);
		newBananaWeapon(player);
		new PlayerAi(player, messages, fov);
		return player;
	}
	
	//CREATING CREATURES
	public Creature newFungus(int depth) {
		Creature fungus = new Creature(world, 'f', AsciiPanel.green, 10, 0, 0, 1, "fungus");
		world.addAtEmptyLocation(fungus, depth);
		new FungusAi(fungus, this);
		return fungus;
	}
	
	public Creature newFungusChild(int depth) {
		Creature fungusChild = new Creature(world, 'f', AsciiPanel.brightGreen, 10, 0, 0, 1, "fungus child");
		world.addAtEmptyLocation(fungusChild, depth);
		new FungusChildAi(fungusChild);
		return fungusChild;
	}
	
	public Creature newRockerOnDrugs(int depth) {
		Creature meatalhead = new Creature(world, 'w', AsciiPanel.brightYellow, 14, 5, 0, 15, "rocker on drugs");
		world.addAtEmptyLocation(meatalhead, depth);
		new RockerOnDrugsAi(meatalhead);
		return meatalhead;
	}
	
	public Creature newZombie(int depth, Creature player){
	      Creature zombie = new Creature(world, 'z', Color.LIGHT_GRAY, 60, 10, 10, 5, "zombie");
	      world.addAtEmptyLocation(zombie, depth);
	      new LivingDeadAi(zombie, player);
	      return zombie;
	  }
	
	public Creature newAssasin(int depth, Creature player){
	      Creature assasin = new Creature(world, (char)155, Color.DARK_GRAY, 70, 25, 3, 20, "assasin");
	      world.addAtEmptyLocation(assasin, depth);
	      new AssasinAi(assasin, player);
	      return assasin;
	  }
	
	public Creature newStinkingHole(int depth, Creature player) {
		Creature stinkingHole = new Creature(world, 'O', Color.RED, 666, 0, 100, 3, "stinking hole");
		world.addAtEmptyLocation(stinkingHole, depth);
		new StinkingHoleAi(stinkingHole, this, player);
		return stinkingHole;
	}
	
	
	/*
	 * ============================  ITEMS  =================================
	 */
	
	//Victory Item
	public Item newVictoryItem(int depth) {
		Item victoryItem = new Item('$', AsciiPanel.brightRed, "suitcase with DOSH");
		world.addAtEmptyLocation(victoryItem, depth);
		return victoryItem;
	}
	
	//Environment
	public Item newCocainumSack(int depth) {
		Item sack = new Item(',', AsciiPanel.brightBlack, "little sack with cocaine");
		world.addAtEmptyLocation(sack, depth);
		return sack;
	}
	
	
	//Weapon
	public Item newDagger(int depth){
	    Item item = new Item((char)108, AsciiPanel.white, "dagger");
	    item.setType("weapon");
	    item.setAttackValue(3);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }
	
	public Item newMagneticDagger(int depth) {
		Item item = new Item((char)108, AsciiPanel.blue, "magnetic dagger");
		item.setType("weapon");
	    item.setAttackValue(4);
	    item.setThrownAttackValue(20);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	}

	  public Item newSword(int depth){
	    Item item = new Item((char)159, AsciiPanel.brightWhite, "sword");
	    item.setType("weapon");
	    item.setAttackValue(7);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }

	  public Item newStaff(int depth){
	    Item item = new Item((char)179, AsciiPanel.yellow, "staff");
	    item.setType("weapon");
	    item.setAttackValue(5);
	    item.setDefenseValue(3);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }

	  public Item randomWeapon(int depth){
	    switch ((int)(Math.random() * 3)){
	    case 0: return newDagger(depth);
	    case 1: return newSword(depth);
	    default: return newStaff(depth);
	    }
	  }
	  
	  public Item newBananaWeapon(int depth) {
		Item item = new Item((char)40, AsciiPanel.brightYellow, "banana");
	    item.setType("weapon");
	    item.setAttackValue(1);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }
	  public Item newBananaWeapon(Creature player) {
		  Item item = new Item((char)40, AsciiPanel.brightYellow, "banana");
		  item.setType("weapon");
		  item.setAttackValue(1);
		  item.setFoodValue(10);
		  player.inventory().add(item);
		  return item;
	  }
	  
	  

	  //Armor
	  public Item newLightArmor(int depth){
	    Item item = new Item('[', Color.LIGHT_GRAY, "tunic");
	    item.setType("armor");
	    item.setDefenseValue(2);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }

	  public Item newMediumArmor(int depth){
	    Item item = new Item('[', AsciiPanel.white, "chainmail");
	    item.setType("armor");
	    item.setDefenseValue(4);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }

	  public Item newHeavyArmor(int depth){
	    Item item = new Item('[', AsciiPanel.brightWhite, "platemail");
	    item.setType("armor");
	    item.setDefenseValue(6);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }
	  
	  public Item newLetherBoots(int depth) {
		  Item item = new Item((char)193, Color.LIGHT_GRAY, "lether boots");
		  item.setDefenseValue(1);
		  item.setType("boots");
		  world.addAtEmptyLocation(item, depth);
		  return item;
	  }
	  
	  public Item newLetherGloves(int depth) {
		  Item item = new Item('*', Color.LIGHT_GRAY, "lether gloves");
		  item.setDefenseValue(1);
		  item.setType("gloves");
		  world.addAtEmptyLocation(item, depth);
		  return item;
	  }
	  
	  public Item newLetherHelmet(int depth) {
		  Item item = new Item('^', AsciiPanel.yellow, "lether helmet");
		  item.setDefenseValue(1);
		  item.setVisionValue(0);
		  item.setType("helmet");
		  world.addAtEmptyLocation(item, depth);
		  return item;
	  }
	  
	  public Item newIronBucket(int depth) {
		  Item item = new Item('^', Color.DARK_GRAY, "iron bucket");
		  item.setDefenseValue(3);
		  item.setVisionValue(-3);
		  item.setType("helmet");
		  world.addAtEmptyLocation(item, depth);
		  return item;
	  }
	  
	  public Item newRingOfProtection(int depth) {
		  Item item = new Item((char)248, Color.LIGHT_GRAY, "ring of protection");
		  item.setDefenseValue(3);
		  item.setType("ring");
		  world.addAtEmptyLocation(item, depth);
		  return item;
	  }
	  
	  public Item newRingOfStrength(int depth) {
		  Item item = new Item((char)248, Color.GREEN, "ring of strength");
		  item.setDefenseValue(3);
		  item.setType("ring");
		  world.addAtEmptyLocation(item, depth);
		  return item;
	  }
	  

	  public Item randomArmor(int depth){
	    switch ((int)(Math.random() * 3)){
	    case 0: return newLightArmor(depth);
	    case 1: return newMediumArmor(depth);
	    default: return newHeavyArmor(depth);
	    }
	  }
	//=======================================================================//
	
	/*
	 * ============================  FOOD  =================================
	 */
	public Item newCookie(int depth) {
		Item item = new Item((char)146, Color.getHSBColor(255,221,117), "cookie");
		item.setType("food");
		item.setFoodValue(1);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Item newApple(int depth) {
		Item item = new Item((char)46, AsciiPanel.brightRed, "apple");
		item.setType("food");
		item.setFoodValue(2);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	//=======================================================================//
		
}
