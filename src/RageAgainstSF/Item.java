package RageAgainstSF;

import java.awt.Color;

public class Item {
	private char glyph;
	private Color color;
	private String name;
	private int foodValue;
	private int attackValue;
	private int defenseValue;
	private int visionValue;
	private String type;
	private int thrownAttackValue;
	
	public char glyph() {
		return glyph;
	}
	public Color color() {
		return color;
	}
	public String name() {
		return name;
	}
	public int foodValue() {
		return foodValue;
	}
	public int attackValue() {
		return attackValue;
	}
	public int defenseValue() { 
		return defenseValue;
	}
	public int visionValue() {
		return visionValue;
	}
	
	public String type() {
		return type;
	}
	
	public void setFoodValue(double amount) { foodValue += amount; }
	public void setAttackValue(int amount) {
		attackValue += amount;
		setThrownAttackValue(amount);
	}
	public void setDefenseValue(int amount) { defenseValue += amount; }
	public void setVisionValue(int amount) { visionValue += amount; }
	public void setType(String type) { this.type = type; }
	
    public int thrownAttackValue() { return thrownAttackValue; }
    public void setThrownAttackValue(int amount) { thrownAttackValue += amount; }
	
	public Item(char glyph, Color color, String name) {
		super();
		this.glyph = glyph;
		this.color = color;
		this.name = name;
		this.type = "environment";
		this.thrownAttackValue = 1;
	}
	public String details() {
		String details = "";

	    if (attackValue != 0)
	        details += "    attack:" + attackValue;

	    if (defenseValue != 0)
	        details += "    defense:" + defenseValue;

	    if (thrownAttackValue != 0)
	        details += "    thrown attack:" + thrownAttackValue;
	    
	    if (foodValue != 0)
	        details += "    food:" + foodValue;
	    
	    return details;
	}
	
}
