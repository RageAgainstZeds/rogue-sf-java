package RageAgainstSF.Screens;

import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class HelpScreen implements Screen {

	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.clear();
		
		int y = 1;
		
		terminal.writeCenter("Mr.Torgue's notes", y++);
		terminal.write("You are in the caves with cocaine factory.", 1, y++);
		terminal.write("Your mission is find the suitcase with dosh and get hell out of here.", 1, y++);
		terminal.write("Exit is the green emergency exit door on the top level.", 1, y++);
		terminal.write("You here cuz you need money for present to Tiny Tina's birthday.", 1, y++);
		terminal.write("Use everything you find to survive, soldier!", 1, y++);
		y++;
		
		
		
		String[] hotkeys = {
				"[g] or [,] to pick up",
				"[d] to drop",
				"[e] to eat",
				"[w] to wear or wield",
				"[r] to put off the rings from your fingers",
				"[?] for help",
				"[x] to examine your items",
				"[;] to look around"
				};
		
		int x = 2;
		
		for (String str : hotkeys) {
			terminal.write(str, x, y);
			y++;
		}
		
		terminal.writeCenter("-- press any key to continue --", terminal.getHeightInCharacters()-2);
		
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		return null;
	}
	
}
