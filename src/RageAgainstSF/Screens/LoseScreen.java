package RageAgainstSF.Screens;

import java.awt.event.KeyEvent;

import asciiPanel.AsciiPanel;

public class LoseScreen implements Screen {
	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.writeCenter("You are escaped from the dangerous caves.", 2);
		terminal.writeCenter("Without thing You came for - possibility to LIFE of Your friend Tiny Tina!", 3);
		terminal.writeCenter("She counts on You! And You failed, Mr.Torgue!", 4);
		terminal.writeCenter("Shame! Shame on You!", 5);
		terminal.writeCenter("COWARD!", 6);
		
		String[] fingerString = {".-.", "|U|", "| |", "| |", "_| |_", " | | | |-.", "/|     ` |", "| |       |", "|         |", "\\         /", "|       |", "|       |"};
		
		for (int i = 0; i < fingerString.length; i++) {
			terminal.writeCenter(fingerString[i], i+7, AsciiPanel.brightRed);
		}
		
		terminal.writeCenter("-- press [Enter] to restart and save your dignity --", 22);
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		return key.getKeyCode() == KeyEvent.VK_ENTER ? new PlayScreen() : this;
	}
}