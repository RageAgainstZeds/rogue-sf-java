package RageAgainstSF.Screens;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import asciiPanel.AsciiPanel;
import RageAgainstSF.WorldBuilder;
import RageAgainstSF.Creature;
import RageAgainstSF.World;
import RageAgainstSF.EnvironmentFactory;
import RageAgainstSF.FieldOfView;
import RageAgainstSF.Item;
import RageAgainstSF.Tile;

public class PlayScreen implements Screen{
	
	private List<String> messages;
	
	private World world;
    private int screenWidth;
    private int screenHeight;
    
    private Creature player;
    
    private FieldOfView fov;
    
    private Screen subscreen;
    
    public int getScrollX() {
        return Math.max(0, Math.min(player.x - screenWidth / 2, world.width() - screenWidth));
    }
    
    public int getScrollY() {
        return Math.max(0, Math.min(player.y - screenHeight / 2, world.height() - screenHeight));
    }
    
    public PlayScreen(){
        screenWidth = 80;
        screenHeight = 21;
        createWorld();
        messages = new ArrayList<String>();
        fov = new FieldOfView(world);
        
        EnvironmentFactory evironmentFactory = new EnvironmentFactory(world);
        createCreatures(evironmentFactory);
        createItems(evironmentFactory);
        
    }
    
    private int randomInt(int maxValue) {
    	
    	return (int) (Math.random() * maxValue);
    }
    
    /*
     * ===== CREATE CREATURES IN THE WORLD ======
     */
    public void createCreatures(EnvironmentFactory creatureFactory) {
    	
    	//create player
    	player = creatureFactory.newPlayer(messages, fov);
    	
    	
    	//create monsters population
    	for (int z = 0; z < world.depth(); z++) {
    		for (int i = 0; i < 14; i++) {
    			creatureFactory.newFungus(z);
    		}
    		
    		for (int i = 0; i < 10; i++) {
    			creatureFactory.newRockerOnDrugs(z);
    		}
    		
    		for (int i = 0; i < z + 3; i++){
    			creatureFactory.newZombie(z, player);
    	    }
		}
    	
    	creatureFactory.newAssasin(world.depth()-1, player);
    	creatureFactory.newStinkingHole(world.depth()-3, player);
    }
    /*
     * ==========================================
     */
    
    /*
     * ======= CREATE ITEMS IN THE WORLD ========
     */
    private void createItems(EnvironmentFactory eFactory) {
    	
    	//add mass items on every floor
    	for(int z = 0; z < world.depth(); z++) {
    		for(int i = 0; i < randomInt(5); i ++) {
    			eFactory.newCocainumSack(z);
    		}
    		
    		for(int i = 0; i < randomInt(3); i ++) {
    			eFactory.newCookie(z);
    		}
    		
    		for(int i = 0; i < randomInt(2); i ++) {
    			eFactory.newApple(z);
    		}
    	}
    	
    	//create victory item
    	eFactory.newVictoryItem(world.depth() - 1);
    	
    	//add weapons and armor
    	eFactory.newDagger(0);

    	eFactory.newBananaWeapon(0);
    	eFactory.newLightArmor(0);
    	eFactory.newLetherBoots(0);
    	eFactory.newLetherGloves(0);
    	eFactory.newLetherHelmet(0);
    	eFactory.newIronBucket(0);

    	eFactory.newRingOfProtection(0);
    	eFactory.newRingOfProtection(0);
    	eFactory.newRingOfProtection(0);
    	eFactory.newRingOfProtection(0);
    	eFactory.newRingOfStrength(0);
    	eFactory.newRingOfStrength(1);
    	eFactory.newRingOfStrength(1);
    	eFactory.newRingOfStrength(1);
    	eFactory.newRingOfProtection(2);
    	eFactory.newRingOfProtection(2);
    	eFactory.newRingOfProtection(2);
    	
    	eFactory.newSword(1);
    	eFactory.newMediumArmor(2);
    	
    	eFactory.newStaff(3);
    	eFactory.newHeavyArmor(3);
    	
    }
    /*
     * ==========================================
     */
    
    
    private void displayMessages(AsciiPanel terminal, List<String> messages) {
    	int top = screenHeight - messages.size();
        for (int i = 0; i < messages.size(); i++){
            terminal.writeCenter(messages.get(i), top + i);
        }
        messages.clear();
    }
    
    private void createWorld(){
        world = new WorldBuilder(90, 32, 4)
              .makeCaves()
              .build();
    }
	
    private void displayTiles(AsciiPanel terminal, int left, int top) {
		fov.update(player.x, player.y, player.z, player.visionRadius());
		
		for (int x = 0; x < screenWidth; x++){
			for (int y = 0; y < screenHeight; y++){
				int wx = x + left;
				int wy = y + top;

				if (player.canSee(wx, wy, player.z))
					terminal.write(world.glyph(wx, wy, player.z), x, y, world.color(wx, wy, player.z));
				else
					terminal.write(fov.tile(wx, wy, player.z).glyph(), x, y, Color.darkGray);
			}
		}
	}
    
	@Override
	public void displayOutput(AsciiPanel terminal) {
		int left = getScrollX();
	    int top = getScrollY();

	    displayTiles(terminal, left, top);
	    
	    displayMessages(terminal, messages);
	    
	    String stats = String.format(" %3d/%3d/ hp %8s %8s %8s", player.hp(), player.maxHp(), hunger(), 
	    								player.weapon() != null ? player.weapon().name() : "",
	    								player.armor() != null ? player.armor().name() : "");
	    
	    terminal.write(player.glyph(), player.x - left, player.y - top);
	    terminal.write(stats, 1, terminal.getHeightInCharacters()-1);
		
		if (subscreen != null)
		    subscreen.displayOutput(terminal);
		
	}
	
	private String hunger() {
		if (player.food() < player.maxFood() * 0.05)
			return "Near death!";
		else if (player.food() < player.maxFood() * 0.1)
			return "Starving";
		else if (player.food() < player.maxFood() * 0.2)
			return "Hungry";
		else if (player.food() < player.maxFood() * 0.8)
			return "Full-bellied";
		else if (player.food() <= player.maxFood() * 0.9)
			return "Pig out";
		else if (player.food() > player.maxFood() * 0.9)
			return "Feel bloated";
		else
			return "";
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		int level = player.level();
	
	     if (subscreen != null) {
	         subscreen = subscreen.respondToUserInput(key);
	     } else {
	         switch (key.getKeyCode()){
	         case KeyEvent.VK_LEFT:
	         case KeyEvent.VK_NUMPAD4:
	         case KeyEvent.VK_H: player.moveBy(-1, 0, 0); break;
	         case KeyEvent.VK_RIGHT:
	         case KeyEvent.VK_NUMPAD6:
	         case KeyEvent.VK_L: player.moveBy( 1, 0, 0); break;
	         case KeyEvent.VK_UP:
	         case KeyEvent.VK_NUMPAD8:
	         case KeyEvent.VK_K: player.moveBy( 0,-1, 0); break;
	         case KeyEvent.VK_DOWN:
	         case KeyEvent.VK_NUMPAD2:
	         case KeyEvent.VK_J: player.moveBy( 0, 1, 0); break;
	         case KeyEvent.VK_NUMPAD7: 
	         case KeyEvent.VK_Y: player.moveBy(-1,-1, 0); break;
	         case KeyEvent.VK_NUMPAD9: 
	         case KeyEvent.VK_U: player.moveBy( 1,-1, 0); break;
	         case KeyEvent.VK_NUMPAD1: 
	         case KeyEvent.VK_B: player.moveBy(-1, 1, 0); break;
	         case KeyEvent.VK_NUMPAD3: 
	         case KeyEvent.VK_N: player.moveBy( 1, 1, 0); break;
	         
	         
	         //Screens
	         case KeyEvent.VK_D: subscreen = new DropScreen(player); break;
	         case KeyEvent.VK_E: subscreen = new EatScreen(player); break;
	         case KeyEvent.VK_W: subscreen = new EquipScreen(player); break;
	         case KeyEvent.VK_R: subscreen = new UnequipRingScreen(player); break;
         }
	        
	         switch (key.getKeyChar()){
	         case 'g':
	         case ',': player.pickup(); break;
	         case '<': 
	        	 if(playerTryToEscape())
	        		 return userEscapes();
	        	 else
	        		 player.moveBy( 0, 0, -1); break;
	         case '>': player.moveBy( 0, 0, 1); break;
	         case '?':  subscreen = new HelpScreen(); break;
	         }
	     }
	     
	     if (player.level() > level)
		      subscreen = new LevelUpScreen(player, player.level() - level);
	    
	     if (subscreen == null)
	         world.update();
	    
	     if (player.hp() < 1)
	         return new LoseScreen();
	    
	     return this;
	 }

	private Screen userEscapes() {
		for (Item item : player.inventory().getItems()) {
			if (item != null && item.name().equals("suitcase with DOSH"))
				return new WinScreen();
		}
		return new LoseScreen();
	}

	private boolean playerTryToEscape() {
		return player.z == 0 && world.tile(player.x, player.y, player.z) == Tile.ESCAPE_STAIRS;
	}
}
