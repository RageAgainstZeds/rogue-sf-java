package RageAgainstSF.Screens;

import java.awt.Color;
import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class StartScreen implements Screen{
	
	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.writeCenter("--= Mr. TORGUE in Octane Caves =--", 3, Color.ORANGE);
		
		String[] explosionArray = {"____", "__,-~~/~    `---.",
				"_/_,---(      ,    )", "__ /        <    /   )  \\___", "- ------===;;;'====------------------===;;;===----- -  -", "\\/  ~\"~\"~\"~\"~\"~\\~\"~)~\"/",
				"(_ (   \\  (     >    \\)", "\\_( _ <         >_>'", "~ `-i' ::>|--\"", "I;|.|.|", "<|i::|i|`.", "(` ^'\"`-' \")", 
				"--------------------------------EXPLOSION--------------------------------"};
		for (int i = 0; i < explosionArray.length; i++) {
			terminal.writeCenter(explosionArray[i], i+4, Color.ORANGE);
		}
		
		terminal.writeCenter("-- press [Enter] to EXPLOSION --", 22);
	}
	
	@Override
	public Screen respondToUserInput(KeyEvent key) {
		return key.getKeyCode() == KeyEvent.VK_ENTER ? new PlayScreen() : this;
	}
}
