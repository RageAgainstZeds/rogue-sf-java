package RageAgainstSF.Screens;

import RageAgainstSF.Creature;
import RageAgainstSF.Item;

public class UnequipRingScreen extends InventoryBasedScreen {

	public UnequipRingScreen(Creature player) {
		super(player);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getVerb() {
		return "unequip from your fingers";
	}

	@Override
	protected boolean isAcceptable(Item item) {
		// TODO Auto-generated method stub
		return item.type().equals("ring");
	}

	@Override
	protected Screen use(Item item) {
		player.unequipRing(item);
		return null;
	}
	
}
