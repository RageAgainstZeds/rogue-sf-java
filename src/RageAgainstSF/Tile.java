package RageAgainstSF;

import asciiPanel.AsciiPanel;
import java.awt.Color;

public enum Tile {
	FLOOR((char)250, AsciiPanel.yellow, "Dirty floor"),
	WALL((char)177, AsciiPanel.yellow, "Dirty wall"),
	BOUNDS('x', AsciiPanel.brightBlack, "Beyond the edge of the world."),
	STAIRS_DOWN('>', AsciiPanel.white, "Staircase that goes down."),
	STAIRS_UP('<', AsciiPanel.white, "Staircase that goes down."),
	UNKNOWN(' ', AsciiPanel.white, " UNKNOWN AREA "),
	ESCAPE_STAIRS((char)219, AsciiPanel.brightGreen, "A escape exit to the surface.");
	
	private char glyph;
	public char glyph() { return glyph;}
	
	private Color color;
	public Color color() {return color;}
	
	private String details;
	public String details() {return details;}

	public void setDetails(String details) {
		this.details = details;
	}

	Tile(char glyph, Color color, String details) {
		this.glyph = glyph;
		this.color = color;
		this.details = details;
	}
	
	public boolean isDiggable() {
		return this == Tile.WALL;
	}
	
	public boolean isGround() {
		return this != Tile.WALL && this != Tile.BOUNDS;
	}
	
	public boolean isEmptyGround() {
		return this != Tile.WALL && this != Tile.BOUNDS && this !=Tile.ESCAPE_STAIRS && this != Tile.STAIRS_DOWN && this != Tile.STAIRS_UP;
	}
	
	
}
